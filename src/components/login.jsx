import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { api, ROUTES, isLogin as login } from "../cfg";
import store from "store";
import { Redirect } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn({ isLogin, setLogin }) {
  const classes = useStyles();

  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [loading, setLoading] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    fetch(`${api}/${ROUTES.LOGIN}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, pass: password }),
    })
      .then((res) => {
        switch (res.status) {
          case 403:
            store.clearAll();
            break;
          case 200:
            res
              .json()
              .then((resp) => {
                store.set("token", resp.token);
                store.set("usuario", {
                  email: resp.email,
                  usuario: resp.usuario,
                  fechaFin: resp.fechaFin,
                });
                setLogin(true);
              })
              .catch((er) => console.log(er))
              .finally(() => setLoading(false));
            break;
          default:
            break;
        }
      })
      .catch((er) => console.log(er))
      .finally(() => setLoading(false));
  };

  let botonSign = () => {
    return loading ? (
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        disabled
        className={classes.submit}
      >
        <CircularProgress size={20} />
        Sign In
      </Button>
    ) : (
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
      >
        Sign In
      </Button>
    )
  }
  
  let cuerpo = (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="current-password"
          />
          {botonSign()}
        </form>
      </div>
      <Box mt={8}></Box>
    </Container>
  );

  return !isLogin ? cuerpo : <Redirect to="/proveedores" />;
}
