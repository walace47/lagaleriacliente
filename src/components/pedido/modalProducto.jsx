import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, InputAdornment, TextField } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    "& > *": {
      margin: theme.spacing(1),
      width: "60ch",
    },
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

export default function AlertDialogProducto( props ) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const [producto,setProducto] = React.useState({
      descripcion:'',
      cantidad:0,
      precioUnitario:0
  })
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleAgregar = () =>{
    //console.log("dadasd")
    //console.log(producto)
    if(Number(producto.cantidad) > 0){
        props.agregarProducto(producto);
        setOpen(false);
    }
  }

  const handleChange = (campo, valor) => {
    console.log(valor.target.value);
    setProducto({
        ...producto,
        [campo]:valor.target.value
    })
  }

  return (
    <React.Fragment>
      <Button
        size="small"
        onClick={handleClickOpen}
        variant="contained"
        color="primary"
      >
        AGREGAR
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Agregar un nuevo producto"}
        </DialogTitle>
        <DialogContent>
            <form className={classes.root} noValidate autoComplete="off">
              <Grid container>
                <Grid item xs={12}>
                  <TextField
                    id="standard-full-width"
                    label="Descripcion"
                    fullWidth
                    margin="normal"
                    onChange={(e) => handleChange('descripcion',e) }
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField 
                    onChange={(e) => handleChange('cantidad',e)}
                    className={classes.textField} 
                    label="Cantidad" />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    onChange={(e) => handleChange('precioUnitario',e)}
                    className={classes.textField}
                    label="Precio unitario"
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">$</InputAdornment>
                        ),
                      }}
                  />
                </Grid>
              </Grid>
            </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleAgregar} color="primary">
            Agregar
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cancelar
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
