import React,{useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Box, Button, Grid, TableFooter } from "@material-ui/core";
import ModalProducto from './modalProducto';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function BasicTable(props) {
  const classes = useStyles();
  const [productos,setProductos] =  useState([])

  let actualizarTabla = (nuevoProducto) =>{
    console.log(nuevoProducto)
    setProductos([...productos,nuevoProducto])
  }
  return (

    <React.Fragment>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Descripcion</TableCell>
            <TableCell align="right">Cantidad</TableCell>
            <TableCell align="right">Precio unitario</TableCell>
            <TableCell align="right">Total</TableCell>
            <TableCell align="right">Acciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {productos.map((row,i) => (
            <TableRow key={i}>
              <TableCell component="th" scope="row">
                {row.descripcion}
              </TableCell>
              <TableCell align="right"> {row.cantidad}</TableCell>
              <TableCell align="right">$ {row.precioUnitario}</TableCell>
              <TableCell align="right">
                $ {row.cantidad * row.precioUnitario}
              </TableCell>
              <TableCell align="right">Acciones</TableCell>
            </TableRow>
          ))}
        </TableBody>
        
      </Table>
    </TableContainer>
    <ModalProducto agregarProducto={actualizarTabla} />
    </React.Fragment>

  );
}
