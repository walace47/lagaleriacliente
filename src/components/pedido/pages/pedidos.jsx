import React, { useState, useEffect } from "react";
import { api, ROUTES, header } from "../../../cfg";
import Button from "@material-ui/core/Button";
import Tabla from "../tabla";
import { Grid, LinearProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import NuevoPedido from "./nuevoPedido";
import { Link, Redirect } from "react-router-dom";
import store from "store";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box width={1} p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const PedidosPage = (props) => {
  // Declare a new state variable, which we'll call "count"
  let [pedidos, setPedidos] = useState([]);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  let [auth, setAuth] = useState(true);
  const [loading,setLoading] = React.useState(true);


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    console.log(header())
    fetch(`${api}/${ROUTES.PEDIDO}`, {
      method: "get",
      ...header(),
    })
      .then((res) => {
        switch (res.status) {
          case 401:
            setAuth(false);
            store.clearAll();
            break;
          default:
            res.json()
            .then((result) => {
              setLoading(false);
              setPedidos(result);
            })
            .catch((er) => console.log(er));
        }
      })
      .catch((er) => console.log(er));
  }, []);


  const components = (
    <React.Fragment>
      <Grid item xs={12}>
      <Link to={`/pedido/-1`}>
        <Button variant="contained" color="primary">
          Nuevo Pedido
        </Button>
      </Link>

      </Grid>
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab label="Pendientes" {...a11yProps(0)} />
            <Tab label="Historico" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Tabla pedidos={pedidos.filter((e) => e.estado == "PENDIENTE")} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Tabla pedidos={pedidos.filter((e) => e.estado == "FINALIZADO")} />
        </TabPanel>
      </div>
    </React.Fragment>
  );

  return (
    <div className={classes.root}>
      {!loading ? components:  <LinearProgress />}
    </div>
  );
};

export default PedidosPage;
