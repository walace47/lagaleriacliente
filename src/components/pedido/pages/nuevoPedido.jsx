import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { Button, FormControl, Grid, InputLabel } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { api, ROUTES, header } from "../../../cfg";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import { Redirect, useParams } from "react-router-dom";

import TablaProductos from "../tablaProductos";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1)

    },
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
   
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function NuevoPedido(props) {
  let { id } = useParams();

  console.log(id);
  const classes = useStyles();
  let [proveedores, setProveedores] = useState([]);
  let [auth, setAuth] = useState(true);

  let [pedido, setPedido] = useState({
    proveedor: {},
    viajante: {},
    productos: [],
    descripcion: "",
    mensaje: "",
  });

  useEffect(() => {
    fetch(`${api}/${ROUTES.PROVEEDOR}`, {
      method: "get",
      ...header(),
    })
      .then((res) => {
        switch (res.status) {
          case 401:
            setAuth(false);
            break;
          default:
            return res
              .json()
              .then((result) => {
                setProveedores(result);
                setPedido({ proveedor: result[0]._id });
              })
              .catch((er) => console.log(er));
        }
      })
      .catch((er) => console.log(er));
  }, []);

  const handleProveedorChange = (event) => {
    setPedido({ ...pedido ,proveedor: event.target.value });
  };

  const components = (
    <React.Fragment>
      <form className={classes.root} noValidate autoComplete="off">
        <Grid item xs={12}>
          <FormControl className={classes.formControl}>
            <InputLabel>Proveedor</InputLabel>
            <Select value={pedido.proveedor} onChange={handleProveedorChange}>
              {proveedores.filter(e => e.tipo == "PROVEEDOR").map((e) => (
                <MenuItem key={e._id} value={e._id}>
                  {e.nombre}
                </MenuItem>
              ))}
            <MenuItem key={-1} value={''}>
                  Ninguno
            </MenuItem>
            </Select>
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel>Viajante</InputLabel>
            <Select value={pedido.viajante} onChange={(e) => setPedido({...pedido,viajante:e.target.value})}>
              {proveedores.filter(e => e.tipo === "VIAJANTE").map((e) => (
                <MenuItem key={e._id} value={e._id}>
                  {e.nombre}
                </MenuItem>
              ))}
            <MenuItem key={-1} value={''}>
                  Ninguno
            </MenuItem>
            </Select>
          </FormControl>

        </Grid>


        <Grid item xs={12}>
          <TablaProductos />
        </Grid>

        <Grid item xs={12}>
          <TextField
            id="standard-full-width"
            label="Mensaje"
            multiline
            rows={4}
            placeholder=""
            helperText="Mensaje para el proveedor"
            fullWidth
            margin="normal"
          />
        </Grid>

        <Grid item xs={12} style={{ paddingTop: "8px" }}>
          <Button variant="contained" color="primary">
            Enviar
          </Button>
        </Grid>
      </form>
    </React.Fragment>
  );
  return (
    <React.Fragment>
      {auth ? components : <Redirect to={"login"} />}
    </React.Fragment>
  );
}
