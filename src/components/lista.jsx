import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import LayersIcon from "@material-ui/icons/Layers";
import { Link } from "react-router-dom";
import store from "store";

const itemLogout = (setLogin) => {
  return (
    <ListItem
      component={Link}
      to="/login"
      onClick={() => {
        store.clearAll();
        setLogin(false);
      }}
      button
    >
      <ListItemIcon>
        <LayersIcon />
      </ListItemIcon>
      <ListItemText primary="Logout" />
    </ListItem>
  );
};

const itemLogin = () => {
  return (
    <ListItem component={Link} to="/login" button>
      <ListItemIcon>
        <LayersIcon />
      </ListItemIcon>
      <ListItemText primary="Login" />
    </ListItem>
  );
};

const MainListItems = ({ login, setLogin }) => {

  return (
    <React.Fragment>
      <ListItem component={Link} to="/proveedores" button>
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Proveedores" />
      </ListItem>

      <ListItem component={Link} to="/pedidos" button>
        <ListItemIcon>
          <ShoppingCartIcon />
        </ListItemIcon>
        <ListItemText primary="Pedidos" />
      </ListItem>
      {login ? itemLogout(setLogin) : itemLogin()}
    </React.Fragment>
  );
};

export default MainListItems;
