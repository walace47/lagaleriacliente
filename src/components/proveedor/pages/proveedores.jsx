import React, { useState, useEffect } from "react";
import { api, ROUTES, header } from "../../../cfg";
import Button from "@material-ui/core/Button";
import Tabla from "../tabla";
import { Grid, LinearProgress, makeStyles } from "@material-ui/core";
import { Link, Redirect } from "react-router-dom";
import store from "store";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));


const ProveedorPage = (props) => {
  // Declare a new state variable, which we'll call "count"
  let [proveedores, setProveedores] = useState([]);
  let [auth, setAuth] = useState(true);
  let [loading, setLoading] = useState(true);

  const classes = useStyles();

  useEffect(() => {
    fetch(`${api}/${ROUTES.PROVEEDOR}`, {
      method: "get",
      ...header(),
    })
      .then((res) => {
        switch (res.status) {
          case 401:
            setAuth(false);
            store.clearAll();
            break;
          default:
            res.json()
              .then((result) => {
                setProveedores(result);
                setLoading(false);
              })
              .catch((er) => console.log(er));
        }
      })
      .catch((er) => console.log(er));
  }, []);

  let components = (
    <React.Fragment>
      <Grid item xs={12}>

        <Button component={Link} to={'/proveedor/-1'} variant="contained" color="primary">
          Nuevo proveedor
        </Button>
      </Grid>
      <Grid item xs={12}>
        <Tabla proveedores={proveedores} />
      </Grid>

    </React.Fragment>
  );

  return (
    <div className={classes.root}>
      {!loading ? components:  <LinearProgress />}
    </div>
  );
};

export default ProveedorPage;
