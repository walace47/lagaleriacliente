import React, { Fragment, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { Button, CircularProgress, FormControl, Grid, InputLabel, MenuItem, Select } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';
import { api, ROUTES,header } from "../../../cfg";
import { Redirect } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1)
    },
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function FormularioProveedor(props) {

  const classes = useStyles();
  let [proveedor, setProveedor] = useState({
    nombre: "",
    email: "",
    telefono: "",
    cbu: "",
    tipo:"PROVEEDOR"
  });

  let [loading,setLoading] = useState(false);
  let [error,setError] = useState(false);
  let [mensajeError,setMensajeError] = useState('');
  let [redirect, setRedirect] = useState(false);


  const renderRedirect = () => {
    if (redirect) {
      return <Redirect to='/proveedor' />
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    
    if(proveedor.nombre == ""){
        setError(true);
        setMensajeError("El nombre del proveedor es obligatorio");
    }else if(proveedor.email == ""){
        setError(true);
        setMensajeError("el email del proveedor es obligatorio");
    }else{
      setLoading(true);
      console.log(header())
      fetch(`${api}/${ROUTES.PROVEEDOR}`, {
        method: "POST",
          ...header(),
        body: JSON.stringify(proveedor),
      }).then((res) => {
        res.json()
          .then(() => setRedirect(true)) 
          .catch((e) => {
            setError = true; 
            setMensajeError = "Ubo un error interno en el sistema " + JSON.stringify(e);
          })
      })
      .catch((e) => {
        setError = true; 
        setMensajeError = "Ubo un error interno en el sistema " + JSON.stringify(e);
      })
      .finally(() => loading = false);
    }

  }
  return (
    <form onSubmit={handleSubmit} className={classes.root} noValidate autoComplete="off">
      {renderRedirect()}
      {error ? <Alert variant="filled" severity="error">{mensajeError}</Alert> : <Fragment></Fragment>}
      <Grid item xs={12}>
        <FormControl className={classes.formControl}>
          <InputLabel>Tipo</InputLabel>
          <Select value={proveedor.tipo} onChange={(e) => setProveedor({...proveedor,tipo:e.target.value})  
            }>
              <MenuItem key="VIAJANTE" value="VIAJANTE">
                VIAJANTE
              </MenuItem>
              <MenuItem key="PROVEEDOR" value="PROVEEDOR">
                  PROVEEDOR 
              </MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <TextField id="nombre" value={proveedor.nombre} onChange={(e)=> setProveedor({...proveedor,nombre:e.target.value})} label="Nombre" />
        <TextField id="standard-basic" value={proveedor.telefono} onChange={(e)=> setProveedor({...proveedor,telefono:e.target.value})} label="telefono" />
      </Grid>

      <Grid item xs={12}>
        <TextField id="email" value={proveedor.email} onChange={(e)=> setProveedor({...proveedor,email:e.target.value})} label="Email" />
        <TextField id="cbu" 
          value={proveedor.cbu} 
          onChange={(e)=> setProveedor({...proveedor,cbu:e.target.value})} 
          label="CBU" />
      </Grid>

      <Grid item style={{ paddingTop: "8px" }} xs={12}>
        <Button type="submit" variant="contained" color="primary" disabled={loading}>
          AGREGAR {loading ? <CircularProgress size={20} /> : '' }
        </Button>
      </Grid>
    </form>
  );
}
