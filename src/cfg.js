import store from "store";

export const api = "https://la-galeria-app.herokuapp.com/api";

export const ROUTES = {
  PROVEEDOR: "proveedor",
  PEDIDO: "pedido",
  LOGIN: "login",
};

export function header() {
  return {
    headers: {
      "Content-Type": "application/json",
      token: store.get("token") ? store.get("token") : "",
    },
  };
}

export function isLogin() {
  let usuario = store.get("usuario");
  console.log(store.get("usuario"));
  if (usuario && new Date(usuario.fechaFin) > new Date()) {
    return true;
  } else {
    return false;
  }
}
